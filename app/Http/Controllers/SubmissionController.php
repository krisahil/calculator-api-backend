<?php

namespace App\Http\Controllers;

use App\Models\Submission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class SubmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return [];
    }

    public function indexToday()
    {
        $submissions = DB::table('submissions')
            ->where('created_at', '>=', Carbon::today('America/New_York'))
            ->get();

        return $submissions;
    }

    public function indexByDate($date)
    {
        $submissions = DB::table('submissions')
            ->whereDate('created_at', $date)
            ->get();

        return $submissions;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $submission = new Submission();
        $submission->user_id = isset($request->user_id) ? $request->user_id : '';
        $submission->problem_top = $request->problem_top;
        $submission->problem_bottom = $request->problem_bottom;
        $submission->problem_operator = $request->problem_operator;
        $submission->problem_answer = $request->problem_answer;
        $submission->submitted_answer = $request->submitted_answer;
        $submission->is_correct = $request->is_correct;
        $submission->save();

        return response()->json([
            "message" => "submission record created"
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
