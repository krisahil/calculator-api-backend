<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Preset;

class PresetController extends Controller
{
    public function index() {
        return Preset::all()->map(function ($preset) {
            $row = [
                'id' => $preset->id,
                'name' => $preset->name,
                'minimum' => $preset->minimum_correct,
                'operators' => [],
            ];
            foreach (['add', 'subtract', 'multiply', 'divide'] as $type) {
                if ($preset->{"{$type}_enabled"}) {
                    $row['operators'][] = [
                        'operator' => $type,
                        'top' => [
                            'start' => $preset->{"{$type}_top_start"},
                            'end' => $preset->{"{$type}_top_end"},
                        ],
                        'bottom' => [
                            'start' => $preset->{"{$type}_bottom_start"},
                            'end' => $preset->{"{$type}_bottom_end"},
                        ],
                    ];
                }
            }

            return $row;
        });
    }
}
