<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Preset extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'add_enabled',
        'add_top_start',
        'add_top_end',
        'add_bottom_start',
        'add_bottom_end',
        'substract_enabled',
        'substract_top_start',
        'substract_top_end',
        'substract_bottom_start',
        'substract_bottom_end',
        'multiply_enabled',
        'multiply_top_start',
        'multiply_top_end',
        'multiply_bottom_start',
        'multiply_bottom_end',
        'divide_enabled',
        'divide_top_start',
        'divide_top_end',
        'divide_bottom_start',
        'divide_bottom_end',
        'minimum_correct',
    ];
}
