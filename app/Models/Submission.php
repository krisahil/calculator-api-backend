<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'problem_top',
        'problem_bottom',
        'problem_operator',
        'problem_answer',
        'submitted_answer',
        'is_correct',
    ];
}
