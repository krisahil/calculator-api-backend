<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
//use App\Models\Preset;
use App\Http\Controllers\PresetController;
use App\Http\Controllers\SubmissionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware('auth.basic');

Route::get('/token', function (Request $request) {
    $token = $request->session()->token();

    return $token;
})->middleware('auth.basic');

Route::resource('api/presets', PresetController::class);
Route::resource('api/submissions', SubmissionController::class);

Route::get('/api/submissions-today', [SubmissionController::class, 'indexToday']);
Route::get('/api/submissions-by-date/{date}', [SubmissionController::class, 'indexByDate']);
