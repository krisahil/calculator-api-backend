## Install steps

- Install dependencies:
```
npm install
composer install
```

- Install MySQL server (`brew install mysql`)
  - Better yet, why not sqlite?
- Create a database and configure user and password.
- Set the DB connection details in `.env`:
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=[db name]
DB_USERNAME=[db username]
DB_PASSWORD=[db password]
```
- Install the DB schemas: `php artisan migrate`
- Set verbose logs, also in `.env`:
```
LOG_CHANNEL=stack
LOG_LEVEL=debug
```
- Start the local PHP-based web server: `php artisan serve`

## Shame notes

Because I haven't yet figured out how to do authentication or CSRF token, you
may want to use this patch. Otherwise, you'll get http 419 response when you
post to `/api/submissions`.

```
diff --git a/app/Http/Kernel.php b/app/Http/Kernel.php
index 30020a5..4072084 100644
--- a/app/Http/Kernel.php
+++ b/app/Http/Kernel.php
@@ -35,7 +35,7 @@ class Kernel extends HttpKernel
             \Illuminate\Session\Middleware\StartSession::class,
             // \Illuminate\Session\Middleware\AuthenticateSession::class,
             \Illuminate\View\Middleware\ShareErrorsFromSession::class,
-            \App\Http\Middleware\VerifyCsrfToken::class,
+//            \App\Http\Middleware\VerifyCsrfToken::class,
             \Illuminate\Routing\Middleware\SubstituteBindings::class,
         ],
```