<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBaseColumnsToPresetsMoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('presets', function (Blueprint $table) {
            $table->boolean('subtract_enabled');
            $table->integer('subtract_top_start');
            $table->integer('subtract_top_end');
            $table->integer('subtract_bottom_start');
            $table->integer('subtract_bottom_end');
            $table->boolean('multiply_enabled');
            $table->integer('multiply_top_start');
            $table->integer('multiply_top_end');
            $table->integer('multiply_bottom_start');
            $table->integer('multiply_bottom_end');
            $table->boolean('divide_enabled');
            $table->integer('divide_top_start');
            $table->integer('divide_top_end');
            $table->integer('divide_bottom_start');
            $table->integer('divide_bottom_end');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('presets', function (Blueprint $table) {
            //
        });
    }
}
