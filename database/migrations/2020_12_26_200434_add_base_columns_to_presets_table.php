<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBaseColumnsToPresetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('presets', function (Blueprint $table) {
            $table->boolean('add_enabled');
            $table->integer('add_top_start');
            $table->integer('add_top_end');
            $table->integer('add_bottom_start');
            $table->integer('add_bottom_end');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('presets', function (Blueprint $table) {
            //
        });
    }
}
